@extends('layouts.app')
@section('content')
<div class="docwell-admin contract">
	<div class="container contract-page">
		<div class="questions-block">
			<div class="questions-container">
			<form action="/admin/forms/{{ $form->id }}/edit" method="POST">
				<div class="question">
					<input type="radio" checked="checked" name="required" id="required-answer" value="{{ $form->required }}"><label for="required-answer">Обов'язкова відповідь на запитання</label><br>
					<h1>Введіть питання:</h1>
					<input type="text" id="question-name" name="question" placeholder="Приклад: Оберіть потрібні Вам послуги" value="{{ $form->question }}">
					<input type="text" name="hint" placeholder="+ Додати коментар" value="{{ $form->hint }}">
				</div>
				<div class="fill-field-form">
					<div class="question">
						<h1>Введіть приклад заповнення</h1>
						<input type="text" name="fill-example" value="{{ $form->example }}">
					</div>
					<div class="question">
						<h1>Формат введення данних</h1>
						@foreach ($formFormats as $formFormat)
							<input type="radio" name="answer_format" id="format{{ $formFormat->id }}"><label for="format{{ $formFormat->id }}">{{ $formFormat->name }}</label>
						@endforeach
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
@endsection