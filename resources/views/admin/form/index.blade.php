@extends('layouts.app')
@section('content')
    <div class="docwell-admin contract">
		<header class="admin">
			<a href="#" class="logo"></a>
			<ul>
				<li class="nav-item text-uppercase"><a href="#">сформовані контракти<span class="badge text-center">42</span></a></li>
				<li class="nav-item text-uppercase"><a href="#" class="active-tab">шаблон контракту</a></li>
			</ul>
		</header>
		<section class="container-fluid contract-page">
			<div class="row">
				<div class="col-sm-4 col-md-4">
					<div class="questions-block" id="all-questions">
						<h1 class="title">Питання до користувачів</h1>
						<div class="listing-container">
							<ol class="sortcontainer">
							  <li class="item nondraggable">Вітаю! Як Вас звати?</li>
							  <li class="item nondraggable">Дуже приємно, ___________. Вкажіть телефонний номер</li>
							  <div class="sorting-available">
								@foreach ($forms as $key => $form)
							  		<a href="/admin/forms/{{ $form->id }}/edit" class="choose-question"><li class="item">{{ $form->question }}</li></a>
							  	@endforeach
							  </div>
							  <li class="item nondraggable">Контракт майже сформонвано.</li>
							</ol>
							<div id="add-question">
								Додати питання
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection