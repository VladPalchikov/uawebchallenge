@extends('layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="/admin/forms">
    	<div class="row">
    		<div class="col-lg-4">
    			@if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    	<input type="checkbox" name="required">
		    	<input type="text" name="question" placeholder="Question">
		    	<input type="text" name="hint" placeholder="Hint">
		    	<input type="text" name="example" placeholder="Example">
				
				@foreach($formTypes as $formType)
					<input type="radio" name="type" value="{{ $formType->id }}"> {{ $formType->name }} <br>
				@endforeach

				@foreach($formFormats as $formFormat)
					<input type="radio" name="answer_format" value="{{ $formFormat->id }}"> {{ $formFormat->name }} <br>
				@endforeach

				<input type="submit" value="Save">
    		</div>

    		<div class="col-lg-8">
    			<textarea class="document"></textarea>
    		</div>
    	</div>
    </form>
</div>
@endsection
