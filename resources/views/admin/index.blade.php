@extends('layouts.app')

@section('content')
	<div class="docwell-admin">
	   	<header class="admin">
			<div class="logo"></div>
			<ul>
				<li class="nav-item text-uppercase"><a href="#" class="active-tab">сформовані контракти<span class="badge text-center">42</span></a></li>
				<li class="nav-item text-uppercase"><a href="#">шаблон контракту</a></li>
			</ul>
			<form action="/" class="search-field">
				<input type="search">
				<div class="search-icon"></div>
			</form>
		</header>
		<section class="container-fluid main-content">
			<div class="row">
				<div id="small-dialog" class="zoom-anim-dialog mfp-hide text-center">
					<h1>Цукерка петро олексійович</h1>
					<span class="text-uppercase">телефон</span>
					<p>+380 99 123 45 34</p>
					<span class="text-uppercase">адреса</span>
					<p>м.Київ, вул. Героїв Слави, буд. 25, кв.34</p>
					<span class="text-uppercase">країна подорожі</span>
					<p>Австрія</p>
					<span class="text-uppercase">кількість днів</span>
					<p>65</p>
					<span class="text-uppercase">послуги</span>
					<p>Транспортування: <span>Літаком</span></p>
					<p>Проживання: <span>Готель</span></p>
					<p>Харчування: <span>Шведський стіл (BB)</span></p>
					<span class="text-uppercase">додаткові послуги</span>
					<p>Медичні</p>
					<p>Сувенірна продукція</p>
					<p>Спортивні</p>
					<span class="text-uppercase">страхування</span>
					<p>Життя</p>
					<p>Багажу</p>
					<p>Від невиїзду</p>
					<span class="text-uppercase">супутники</span>
					<p class="capitalize">Цукерка Марина Равіновна</p>
					<div class="contract-icon-wrapper">
						<a href="#">
							<div class="contract-icon">
								<div class="search-hover-icon disabled animated"></div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-3 folder-container">
					<div class="folder-wrapper text-uppercase">
						<a class="popup-with-move-anim" href="#small-dialog">
							<div class="folder">
								<div class="date">18 березня</div>
								<div class="name">цукерка петро олексійович</div>
							</div>
							<div class="folder-hover">
								<div class="phone">+380 000 999 99 99</div>
								<div class="country">австрія</div>
								<div class="days">65 днів</div>
								<div class="details">деталі</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-3 folder-container">
					<div class="folder-wrapper text-uppercase">
						<a class="popup-with-move-anim" href="#small-dialog">
							<div class="folder">
								<div class="date">18 березня</div>
								<div class="name">цукерка петро олексійович</div>
							</div>
							<div class="folder-hover">
								<div class="phone">+380 000 999 99 99</div>
								<div class="country">австрія</div>
								<div class="days">65 днів</div>
								<div class="details">деталі</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-3 folder-container">
					<div class="folder-wrapper text-uppercase">
						<a class="popup-with-move-anim" href="#small-dialog">
							<div class="folder">
								<div class="date">18 березня</div>
								<div class="name">цукерка петро олексійович</div>
							</div>
							<div class="folder-hover">
								<div class="phone">+380 000 999 99 99</div>
								<div class="country">австрія</div>
								<div class="days">65 днів</div>
								<div class="details">деталі</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-3 folder-container">
					<div class="folder-wrapper text-uppercase">
						<a class="popup-with-move-anim" href="#small-dialog">
							<div class="folder">
								<div class="date">18 березня</div>
								<div class="name">цукерка петро олексійович</div>
							</div>
							<div class="folder-hover">
								<div class="phone">+380 000 999 99 99</div>
								<div class="country">австрія</div>
								<div class="days">65 днів</div>
								<div class="details">деталі</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-3 folder-container">
					<div class="folder-wrapper text-uppercase">
						<a class="popup-with-move-anim" href="#small-dialog">
							<div class="folder">
								<div class="date">18 березня</div>
								<div class="name">цукерка петро олексійович</div>
							</div>
							<div class="folder-hover">
								<div class="phone">+380 000 999 99 99</div>
								<div class="country">австрія</div>
								<div class="days">65 днів</div>
								<div class="details">деталі</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-3 folder-container">
					<div class="folder-wrapper text-uppercase">
						<a class="popup-with-move-anim" href="#small-dialog">
							<div class="folder">
								<div class="date">18 березня</div>
								<div class="name">цукерка петро олексійович</div>
							</div>
							<div class="folder-hover">
								<div class="phone">+380 000 999 99 99</div>
								<div class="country">австрія</div>
								<div class="days">65 днів</div>
								<div class="details">деталі</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-3 folder-container">
					<div class="folder-wrapper text-uppercase">
						<a class="popup-with-move-anim" href="#small-dialog">
							<div class="folder">
								<div class="date">18 березня</div>
								<div class="name">цукерка петро олексійович</div>
							</div>
							<div class="folder-hover">
								<div class="phone">+380 000 999 99 99</div>
								<div class="country">австрія</div>
								<div class="days">65 днів</div>
								<div class="details">деталі</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-4 col-md-3 folder-container">
					<div class="folder-wrapper text-uppercase">
						<a class="popup-with-move-anim" href="#small-dialog">
							<div class="folder">
								<div class="date">18 березня</div>
								<div class="name">цукерка петро олексійович</div>
							</div>
							<div class="folder-hover">
								<div class="phone">+380 000 999 99 99</div>
								<div class="country">австрія</div>
								<div class="days">65 днів</div>
								<div class="details">деталі</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection
