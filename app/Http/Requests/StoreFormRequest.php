<?php

namespace Docwell\Http\Requests;

use Docwell\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class StoreFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'required' => 'accepted',
            'question' => 'required',
            'example' => 'required',
            'type' => 'required|integer',
            'answer_format' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'accepted' => 'Невірний формат даних',
            'integer' => 'Невірний формат даних',
            'question.required' => 'Поле "Питання" обов\'язкове для заповнення',
            'example.required' => 'Поле "Приклад" обов\'язкове для заповнення',
            'type.required' => 'Поле "Тип форми" обов\'язкове для заповнення',
            'answer_format.required' => 'Поле "Формат данних" обов\'язкове для заповнення',
        ];
    }
}
