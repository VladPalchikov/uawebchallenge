<?php

namespace Docwell\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Docwell\Http\Requests;
use Docwell\Http\Controllers\Controller;
use Docwell\FormVariant;

class VariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return FormVariant::where('form_id', '=', $request->get('form_id'))->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $variant = new FormVariant;
        $variant->form_id = $request->get('form_id');
        $variant->parent_id = $request->has('parent_id') ? $request->get('parent_id') : null;
        $variant->name = $request->get('name');
        $variant->position = $request->get('position');

        $variant->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return FormVariant::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $variant = FormVariant::find($id);
        $variant->form_id = $request->get('form_id');
        $variant->parent_id = $request->has('parent_id') ? $request->get('parent_id') : null;
        $variant->name = $request->get('name');
        $variant->position = $request->get('position');

        $variant->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $variant = FormVariant::find($id);
        $variant->delete();
    }
}
