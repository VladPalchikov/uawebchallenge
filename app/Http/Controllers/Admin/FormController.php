<?php

namespace Docwell\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Docwell\Http\Requests;
use Docwell\Http\Requests\StoreFormRequest;
use Docwell\Http\Controllers\Controller;
use Docwell\Form;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forms = Form::all();

        return view('admin.form.index', compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $formTypes = \Docwell\FormType::all();
        $formFormats = \Docwell\FormFormat::all();
        return view('admin.form.create', compact('formTypes', 'formFormats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = new \Docwell\Form;
        $form->question = $request->get('question');
        if (!empty($form->question)) {
            $form->save();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = \Docwell\Form::find($id);
        $formTypes = \Docwell\FormType::all();
        $formFormats = \Docwell\FormFormat::all();

        $form->variants = FormVariant::where('form_id', '=', $form->id)->get();

        return view('admin.form.edit', compact('form', 'formTypes', 'formFormats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreFormRequest $request, $id)
    {
        $form = \Docwell\Form::find($id);
        $form->question = $request->get('question');
        $form->hint = $request->get('hint');
        $form->example = $request->get('example');
        $form->required = $request->get('required');
        $form->type = $request->get('type');
        $form->answer_format = $request->get('answer_format');
        $form->save();

        return redirect('/admin/forms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = \Docwell\Form::find($id);
        $form->delete();
    }
}
