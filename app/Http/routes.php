<?php

Route::get('/', 'IndexController');

Route::group(['middleware' => 'web', 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('/', function() {
    	return view('admin.index');
    });

    Route::resource('/forms', 'FormController');
});


Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});