<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(FormSeeder::class);
        $this->call(FormTypeSeeder::class);
        $this->call(FormFormatSeeder::class);
    }
}
