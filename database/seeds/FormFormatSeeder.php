<?php

use Illuminate\Database\Seeder;

class FormFormatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Docwell\FormFormat::truncate();
        $types = [
        	'Літери та цифри',
        	'Тільки літери',
        	'Тільки цифри'
        ];

        foreach ($types as $type) {
        	Docwell\FormFormat::create([
        		'name' => $type
        	]);
        }
    }
}
