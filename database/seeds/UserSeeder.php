<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Docwell\User::truncate();
        Docwell\User::create([
        	'email' => 'admin@gmail.com',
        	'name' => 'Admin',
        	'login' => 'admin',
        	'password' => bcrypt('admin')
        ]);
    }
}
