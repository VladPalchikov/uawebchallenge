<?php

use Illuminate\Database\Seeder;

class FormTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Docwell\FormType::truncate();
        $types = [
        	'Поле вводу',
        	'Вибір з декількох',
        	'Перелік з вводом'
        ];

        foreach ($types as $type) {
        	Docwell\FormType::create([
        		'name' => $type
        	]);
        }
    }
}
