<?php

use Illuminate\Database\Seeder;

class FormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Docwell\Form::truncate();
    	factory(Docwell\Form::class, 10)->create();
    }
}
