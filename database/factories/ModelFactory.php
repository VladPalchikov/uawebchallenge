<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Docwell\Template::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name
    ];
});

$factory->define(Docwell\Form::class, function (Faker\Generator $faker) {
    return [
        'question' => $faker->word,
        'hint' => $faker->word,
        'example' => $faker->word
    ];
});
