<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_variants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('form_id');
            $table->integer('parent_id')->nullable();
            $table->string('name');
            $table->smallInteger('position')->default(0);
            $table->smallInteger('icon')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('form_variants');
    }
}
