jQuery(document).ready(function($) {
	tinymce.init({ 
		selector: '.document',
		plugins: ''
	});
});

$(function() {

	// button disabled

	$('input[type=text], input[type=tel], input[type=number]').keyup(function(){
  		var value = $(this).val(),
  			button = $(this).parent().find('.next-btn-wrapper').find('button'),
  			buttonWrapper = $(this).parent().find('.next-btn-wrapper');
  		if(value) {
  			button.removeAttr('disabled');
  			buttonWrapper.removeClass('next-btn-disabled');
  		} else {
  			button.attr('disabled', 'disabled');	
  			buttonWrapper.addClass('next-btn-disabled');
  		}
  	});

  	$('input[type=number]').click(function(){
  		var value = $(this).val(),
  			button = $(this).parent().find('.next-btn-wrapper').find('button'),
  			buttonWrapper = $(this).parent().find('.next-btn-wrapper');
  		if(value) {
  			button.removeAttr('disabled');
  			buttonWrapper.removeClass('next-btn-disabled');
  		} else {
  			button.attr('disabled', 'disabled');	
  			buttonWrapper.addClass('next-btn-disabled');
  		}
  	});

  	$('input[type=radio], input[type=checkbox]').click(function(){
  		var value = $(this).prop("checked"),
  			button = $(this).parents('form').find('button'),	
  			buttonWrapper = $(this).parents('form').find('.next-btn-wrapper');
  		if(value) {
  			button.removeAttr('disabled');
  			buttonWrapper.removeClass('next-btn-disabled');
  		} else {
  			button.attr('disabled', 'disabled');	
  			buttonWrapper.addClass('next-btn-disabled');
  		}
  	});

  	// scroll
  	var waypoints = $('#user-name').waypoint(function(direction) {
	  	$(this.element).find('input[type=text]').focus();
	  	$('.point').removeClass('selected');
	  	$('#timeline-name').addClass('active selected');
	}, {
	  offset: '10%'
	})


	$('.phone-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-phone').offset().top - 110,
			userFullName = $('.user-name-field').val(),
			userName;	
		userName = userFullName.split(' ');
		console.log(userName[1]);
		if(userName[1]) {
			$('.user-name').text(userName[1] + '.');
		} else {
			$('.user-name').text('незнайомець.');
		};
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-phone').addClass('active selected');
		$('#timeline-phone').parent().find('.line').addClass('active-line');
		$('#timeline-phone').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
		var waypoints = $('#user-phone').waypoint(function(direction) {
		  	$(this.element).find('input[type=tel]').focus();
		  	$('.point').removeClass('selected');
		  	$('#timeline-phone').addClass('active selected');
		}, {
		  offset: '50%'
		})
	});

	$('.adress-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-adress').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-adress').addClass('active selected');
		$('#timeline-adress').parent().find('.line').addClass('active-line');
		$('#timeline-adress').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
		$('.call-wrapper').addClass('fadeInDown').css('visibility', 'visible');
		var waypoints = $('#user-adress').waypoint(function(direction) {
		  	$(this.element).find('input[type=text]').focus(); 
		  	$('.point').removeClass('selected');
		  	$('#timeline-adress').addClass('active selected');
		}, {
		  offset: '50%'
		})
	});
	
	$('.country-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-country').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-country').addClass('active selected');
		$('#timeline-country').parent().find('.line').addClass('active-line');
		$('#timeline-country').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
		var waypoints = $('#user-country').waypoint(function(direction) {
		  	$(this.element).find('input[type=text]').focus(); 
		  	$('.point').removeClass('selected');
		  	$('#timeline-country').addClass('active selected');
		}, {
		  offset: '50%'
		})
	});

	$('.count-days-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-count-days').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-count-days').addClass('active selected');
		$('#timeline-count-days').parent().find('.line').addClass('active-line');
		$('#timeline-count-days').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
		var waypoints = $('#user-count-days').waypoint(function(direction) {
		  	$(this.element).find('input[type=number]').focus(); 
		}, {
		  offset: '50%'
		})
	});

	$('.service-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-service').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-service').addClass('active selected');
		$('#timeline-service').parent().find('.line').addClass('active-line');
		$('#timeline-service').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
		var waypoints = $('#user-service').waypoint(function(direction) {
		  	$(this.element).find('label').click(function(event) {
		  		$(document).keypress(function(e) {
				    if(e.which == 13) {
				    	$('#user-service').next('section').removeClass('disabled');
				    	position = $('#user-aditional-services').offset().top - 110;
				        $.scrollTo(position, 800);
				        $('.point').removeClass('selected');
						$('#timeline-aditional-services').addClass('active selected');
						$('#timeline-aditional-services').parent().find('.line').addClass('active-line');
						$('#timeline-aditional-services').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
				    };
				}); 
		  	});
		}, {
		  offset: '50%'
		})
	});


	$('.aditional-services-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-aditional-services').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-aditional-services').addClass('active selected');
		$('#timeline-aditional-services').parent().find('.line').addClass('active-line');
		$('#timeline-aditional-services').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
		var test = $('#aditional-services').waypoint(function(direction) {
		  	$(document).keypress(function(e) {
		  		alert('ddd');
		  	});
		}, {
		  offset: '50%'
		})
	});

	$('.insurance-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-insurance').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-insurance').addClass('active selected');
		$('#timeline-insurance').parent().find('.line').addClass('active-line');
		$('#timeline-insurance').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});

	$('.friends-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-friends').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-friends').addClass('active selected');
		$('#timeline-friends').parent().find('.line').addClass('active-line');
		$('#timeline-friends').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});

	$('.final-step-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-final-step').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-final-step').addClass('active selected');
		$('#timeline-final-step').parent().find('.line').addClass('active-line');
		$('#timeline-final-step').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});

	$('#timeline-name').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-name').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-phone').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-phone').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-adress').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-adress').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-country').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-country').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-count-days').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-count-days').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-service').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-service').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-aditional-services').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-aditional-services').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-insurance').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-insurance').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-friends').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-friends').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-final-step').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-final-step').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	// animation

	$('.add-friend-btn').hover(function() {
		$(this).addClass('flip');
	}, function() {
		$(this).removeClass('flip');
	});

	$('.call-wrapper').hover(function() {
		$('.phone-icon').addClass('wobble');
	}, function() {
		$('.phone-icon').removeClass('wobble');
	});

	$('.service-title').click(function() {
		$(this).parent().find('ul').removeClass('disabled').addClass('fadeInRight');
	});

	$('.service-item label').click(function() {
		var list = $(this).parents('ul');
			select = $(this).parents('.service-item').find('.selected-answer');
		select.removeClass('disabled').addClass('fadeInRight');
		list.removeClass('fadeInRight').addClass('fadeOutRight');
		setTimeout(function() { 
			list.addClass('disabled').removeClass('fadeOutRight') }, 400);
	});

	$('.contract-icon').hover(function() {
		$('.search-hover-icon').removeClass('disabled').addClass('zoomIn');
	}, function() {
		$('.search-hover-icon').removeClass('zoomIn').addClass('zoomOut');
		setTimeout(function() { 
			$('.search-hover-icon').addClass('disabled').removeClass('zoomOut') }, 400);
	});

   	// shape contract

   	$('.shape').click(function(event) {
   		$('.contract-done').removeClass('disabled');
   	});

	// timeline
	$('.point').click(function() {

		if ($(this).hasClass('active')) {
			$('.point').removeClass('selected');
			$(this).addClass('selected');
		};
	});

	// checkbox

	$('.service-item label').click(function(event) {
		var checkboxValue = $(this).text();
		$(this).parents('.service-item').find('.selected-answer label').text(checkboxValue);
	});

	// add friend

	$('.add-friend-btn').click(function() {
		var inputValue = $('.user-friend').val();

		if (inputValue) {
			$('.friend-list').append("<li class='capitalize animated zoomInDown'>"+inputValue+"</li>");
			$('.user-friend').val('');
		};
	});
	
	// popup 
	
	$('.popup-with-move-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,
		
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom'
	});

	$('.sorting-available').sortable({
        helper: "clone",
        axis: "y",

        revert: 100,
        distance: 0,
        forceHelperSize: true,
        forcePlaceholderSize: true,
        scrollSensitivity: 0,
        start: function(event, ui){
            ui.placeholder.width(ui.helper.width());
        },
    cancel: '.nondraggable, .fixed,input',
    delay: 100,
        stop: function (event, ui) {
           
        },
        change: function (e, ui) { 
            console.log(e);
        },
        tolerance: "pointer"
    });

	$('#add-question').bind('click', function(){
		$('.sorting-available').append('<li class="item"><form action=""><input type="text"></input><input class="submit" type="submit" value=""></form></div></li>');
		$('.sorting-available li:last-child input[type="text"]').focus().blur(function(event) {
			var name = $(this).val();

			$.ajax({
				url: '/admin/forms',
				type: 'POST',
				data: {
					question: name
				},
			});
			
		});;
	});

});
//# sourceMappingURL=app.js.map
