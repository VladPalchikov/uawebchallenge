$(function() {

	// button disabled

	$('input[type=text], input[type=tel], input[type=number]').keyup(function(){
  		var value = $(this).val();
  			button = $(this).parent().find('.next-btn-wrapper').find('button');
  		if(value) {
  			button.removeAttr('disabled');
  		} else {
  			button.attr('disabled', 'disabled');	
  		}
  	});

  	$('input[type=number]').click(function(){
  		var value = $(this).val();
  			button = $(this).parent().find('.next-btn-wrapper').find('button');
  		if(value) {
  			button.removeAttr('disabled');
  		} else {
  			button.attr('disabled', 'disabled');	
  		}
  	});

  	$('input[type=radio]').click(function(){
  		var value = $(this).prop("checked");
  			button = $(this).parents('form').find('button');
  		if(value) {
  			button.removeAttr('disabled');
  		} else {
  			button.attr('disabled', 'disabled');	
  		}
  	});

  	// scroll

	$('.phone-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-phone').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-phone').addClass('active selected');
		$('#timeline-phone').parent().find('.line').addClass('active-line');
		$('#timeline-phone').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});

	$('.adress-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-adress').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-adress').addClass('active selected');
		$('#timeline-adress').parent().find('.line').addClass('active-line');
		$('#timeline-adress').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
		$('.call-wrapper').addClass('fadeInDown').css('visibility', 'visible');
	});
	
	$('.country-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-country').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-country').addClass('active selected');
		$('#timeline-country').parent().find('.line').addClass('active-line');
		$('#timeline-country').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});

	$('.count-days-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-count-days').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-count-days').addClass('active selected');
		$('#timeline-count-days').parent().find('.line').addClass('active-line');
		$('#timeline-count-days').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});

	$('.service-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-service').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-service').addClass('active selected');
		$('#timeline-service').parent().find('.line').addClass('active-line');
		$('#timeline-service').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});


	$('.aditional-services-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-aditional-services').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-aditional-services').addClass('active selected');
		$('#timeline-aditional-services').parent().find('.line').addClass('active-line');
		$('#timeline-aditional-services').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});

	$('.insurance-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-insurance').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-insurance').addClass('active selected');
		$('#timeline-insurance').parent().find('.line').addClass('active-line');
		$('#timeline-insurance').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});

	$('.friends-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-friends').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-friends').addClass('active selected');
		$('#timeline-friends').parent().find('.line').addClass('active-line');
		$('#timeline-friends').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});

	$('.final-step-scroll').click(function(e) {
		$(this).parents('section').next('section').removeClass('disabled');
		var position = $('#user-final-step').offset().top - 110;
		$.scrollTo(position, 800);
		e.preventDefault();
		$('.point').removeClass('selected');
		$('#timeline-final-step').addClass('active selected');
		$('#timeline-final-step').parent().find('.line').addClass('active-line');
		$('#timeline-final-step').parent().find('.point-title').removeClass('disabled').addClass('flipInX');
	});

	$('#timeline-name').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-name').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-phone').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-phone').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-adress').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-adress').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-country').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-country').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-count-days').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-count-days').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-service').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-service').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-aditional-services').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-aditional-services').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-insurance').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-insurance').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-friends').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-friends').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	$('#timeline-final-step').click(function() {
		if ($(this).hasClass('active')) {
			var position = $('#user-final-step').offset().top - 110;
			$.scrollTo(position, 800);
		};
	});

	// animation

	$('.add-friend-btn').hover(function() {
		$(this).addClass('flip');
	}, function() {
		$(this).removeClass('flip');
	});

	$('.call-wrapper').hover(function() {
		$('.phone-icon').addClass('wobble');
	}, function() {
		$('.phone-icon').removeClass('wobble');
	});

	$('.service-title').click(function() {
		$(this).parent().find('ul').removeClass('disabled').addClass('fadeInRight');
	});

	$('.service-item label').click(function() {
		var list = $(this).parents('ul');
			select = $(this).parents('.service-item').find('.selected-answer');
		select.removeClass('disabled').addClass('fadeInRight');
		list.removeClass('fadeInRight').addClass('fadeOutRight');
		setTimeout(function() { 
			list.addClass('disabled').removeClass('fadeOutRight') }, 400);
	});

	// timeline
	$('.point').click(function() {

		if ($(this).hasClass('active')) {
			$('.point').removeClass('selected');
			$(this).addClass('selected');
		};
	});

	// add friend

	$('.add-friend-btn').click(function() {
		var inputValue = $('.user-friend').val();

		if (inputValue) {
			$('.friend-list').append("<li class='animated zoomInDown'>"+inputValue+"</li>");
			$('.user-friend').val('');
		};
	});
});