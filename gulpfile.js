var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix
    .styles(['*.css'])
    .less('app.less')
    .less('admin.less')
    .scripts(['vendor/*.js'], 'public/js/vendor.js')
    .scripts([
    	'app.js'
    ], 'public/js/app.js')
    .version(['public/css/all.css', 'public/css/app.css', 'public/css/admin.css', 'public/js/vendor.js', 'public/js/app.js']);
});
